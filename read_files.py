#!/usr/bin/env python
#read_files.py 
#Clayton Davis
#CSCI-3308 Project

import os
import os.path
import bs4
import requests
import re
import sys
import MySQLdb
from mutagen.id3 import ID3

#Create and open file_data.txt and append 
#Take target directory as argument
def get_basic_info(argv):
	
	directory = str(argv)
	#Get the files within the target directory
	files = os.listdir(directory)

	#Create an empty list to store file info dicts
	files_dict = []
		
	#Iterate over each file
	for f in files:
			
		#Get name of file and split it into base name and extension
		try:
			extension = os.path.splitext(f)[1][1:]
		except: 
			extension = None
			
		base = os.path.splitext(f)[0]
		name = f
		file_path = os.path.abspath(os.path.join(directory, f))
		
		#If the file is an MP3, add artist and album from Mutagen to dictionary.
		if extension == "mp3":
			try: 
				
				audio = ID3(f)
				artist = audio["TPE1"].text[0]
				album = audio["TALB"]
			
				data = {
				"file name" : name,
				"extension" : extension,
				"base name" : base,
				"file path" : file_path,
				"album" : album,
				"artist" : artist 
					}
			#If there are no tags on the file, set tag values to None
			except:		
			
				data = {
				"file name" : name,
				"extension" : extension,
				"base name" : base,
				"file path" : file_path,
				"album" : None,
				"artist" : None
					}
				
			files_dict.append(data)
					
		#Append each file to files_dict
		else:
			data = {
				"file name" : name,
				"extension" : extension,
				"base name" : base,
				"file path" : file_path
					}
			
			files_dict.append(data)	
			
	return files_dict

#Use BS4 to scrape extension info from fileinfo.com
def get_video_extension_library():
	
	#URL of video extensions
	video_url = 'http://www.fileinfo.com/filetypes/video'
	
	#Create BS4 object
	response = requests.get(video_url)
	soup = bs4.BeautifulSoup(response.text)
	
	#Create empty list for extensions
	video_ext = []
	
	#Below finds the extension strings and appends them to the list above
	for tbody in soup.findAll('tbody'):
		for tr in tbody.findAll('tr'):
			
			tds = tr.findAll('td')
			
			ext = tds[0].text
			
			ext = ext.replace(".", "")
			
			video_ext.append(ext)
	
	#Return the list		
	return video_ext
			
#Pretty much the same as above, URLs are just in a list because
#there are many more common image file types.  			
def get_image_extension_library():
	
	image_urls = ['http://www.fileinfo.com/filetypes/3d_image',
	'http://www.fileinfo.com/filetypes/raster_image', 'http://www.fileinfo.com/filetypes/vector_image',
	'http://www.fileinfo.com/filetypes/camera_raw']

	image_ext = []
	
	for image_url in image_urls:
		
		response = requests.get(image_url)
		soup = bs4.BeautifulSoup(response.text)
	
		for tbody in soup.findAll('tbody'):
			
			for tr in tbody.findAll('tr'):
			
				tds = tr.findAll('td')
			
				ext = tds[0].text
			
				ext = ext.replace(".", "")
			
				image_ext.append(ext)
				
	return image_ext
	
def get_music_extension_library():
	
	audio_url = 'http://www.fileinfo.com/filetypes/audio'
	
	audio_ext = []
	
	response = requests.get(audio_url)
	soup = bs4.BeautifulSoup(response.text)
	
	for tbody in soup.findAll('tbody'):
			
		for tr in tbody.findAll('tr'):
			
			tds = tr.findAll('td')
			
			ext = tds[0].text
			
			ext = ext.replace(".", "")
			
			audio_ext.append(ext)
				
	return audio_ext
	
#Add file type to each file dict object	
def append_file_types(files_dict, audio_ext, image_ext, video_ext):
	
	#Iterate over files in files_dict
	for f in files_dict:
		
		#Get the extension of the file
		extension = f["extension"]
		
		#Compare the file extension to the lists of common extensions
		#and apply appropriate 'file type' value for later sorting
		if extension in audio_ext:
			
			f["file type"] = "music"
			f["album"] = "none"
			f["artist"] = "none"
		
		elif extension in image_ext:
			
			f["file type"] = "image"
		
		elif extension in video_ext:
			
			f["file type"] = "video"
			
		else:
			
			f["file type"] = "other"
	
	return files_dict

def data_to_database(files_dict):
	
	#Open databse connection
	db = MySQLdb.connect("sql2.freemysqlhosting.net","sql260291","aS5!eU1*","sql260291" )
	
	#create cursor object
	cursor = db.cursor()
	
	#define queries to clear tables
	clear_video = """truncate table Video""" 
	clear_images = """truncate table Images"""
	clear_music = """truncate table Music""" 
	
	#execute queries to clear tables
	cursor.execute(clear_video)
	cursor.execute(clear_images)
	cursor.execute(clear_music)
	
	#Initiate unique ID variable for each file type
	music_id = 1
	video_id = 1
	image_id = 1
	
	#for each file, add the appropriate info to the proper database table
	for f in files_dict:
		
		if f["file type"] == "music":
			
			album = f["album"]
			artist = f["artist"]
			file_path = f["file path"]
			file_name = f["base name"]
			extension = f["extension"]
			cursor.execute("""INSERT INTO Music VALUES (%s,%s,%s,%s,%s,%s)""",(music_id, artist, album, file_name, extension, file_path))
			
			db.commit()
			
			music_id += 1
		
		elif f["file type"] == "video":
			
			file_path = f["file path"]
			file_name = f["base name"]
			extension = f["extension"]
			
			cursor.execute("""INSERT INTO Video VALUES (%s,%s,%s,%s)""",(video_id, file_name, extension, file_path))
			
			db.commit()
			
			video_id += 1
			
		elif f["file type"] == "image": 
			
			file_path = f["file path"]
			file_name = f["base name"]
			extension = f["extension"]
			
			cursor.execute("""INSERT INTO Images VALUES (%s,%s,%s,%s)""",(image_id, file_name, extension, file_path))
			
			db.commit()
			
			image_id += 1
		
		#if it is not a media file, pass over it and move onto the next
		else:
			
			pass
		
	db.close()
	
#if file is executed directly, run this way	
if __name__ == '__main__':
	
	data_to_database(append_file_types(get_basic_info(sys.argv[1]), get_music_extension_library(), get_image_extension_library(), get_video_extension_library()))
	
	
