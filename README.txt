The Auto-Doc html files are in the "Documentation" folder. Simply open up annotated.html to see Documentation for the entire project. 
The "Media_Testing" folder has some test media files to test reading and sorting. NOTE: They are not actual files (0kb) just empty docs 
named like media, as such, the audo files do not have any ID3 Tags and Artist/Album will come up as "none".

Our main file is "projectGUI.py". To run it, navigate to their directory and type "./projectGUI.py" or "python projectGUI.py" into your terminal. 
This should open the GUI for you to use. NOTE: our program uses wx.python, MySQLdb and BeautifulSoup to run. You can install these 
modules with the following commands:

sudo apt-get install python-wxgtk2.8 python-wxtools wx2.8-i18n
sudo apt-get install python-mysqldb
sudo pip install BeautifulSoup

*Note: wxPython has some bugs that prevented us from being able to include it in the requirements.txt of a pip freeze in a virtual environment.  This made it very difficult for us to cleanly deploy a complete package within the time requirements.  

Once the program is up and running, simply check the boxes corresponding to the type of media you wish to organize, enter the path of the 
folder you wish to organize (this can also be done by clicking browse and selecting the folder), then click the large "Organize Media!" button 
at the bottom of the screen. Once you recieve a "Organization Complete!" confirmation you know your files are done being organized.

To test this code, simply select the "Media_Testing" folder in our repo and watch the magic happen. If you have any questions, I will be 
available to answer them at christopher.flauta@colorado.edu.