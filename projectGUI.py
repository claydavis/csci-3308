#!/usr/bin/env python

## @package projectGUI
#  projectGUI module holds the mainWindow class that is the
#  GUI to our media organizer. It creates a 500x400 frame to hold
#  everything our organizer requires. There are checkboxes for the
#  three media types we will organize as well as a way to select
#  the folder to be organized. It also connects to our database to
#  store file information, a feature that is unneccessary but required
#  under the project requirements. There are functions for each event
#  in the GUI as well as the "organizeMedia" function that pulls
#  everything together.
import wx # Run "sudo apt-get install python-wxgtk2.8 python-wxtools wx2.8-i18n" to install
import MySQLdb # Run "sudo apt-get install python-mysqldb" for lib
import os
import subprocess
import sort
import read_files
import rmtest

## mainWindow class creates the frame from which the users will
#  interact with our program. There will be 3 sections including one
#  that gives users checkboxes for which media the want organized,
#  one to allow users to easily select the folder they want organized,
#  and one that holds a large button that will organized the selected
#  folder based on the users options.
class mainWindow(wx.Frame):
    ## Constructor that needs a parent (can be "None") and a title.
    #  @param parent recieves the frames parent frame (can be "None")
    #  @param title is a string that will be the title of the frame
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(500, 400))
        ## Main panel for the GUI (only 1 panel used)
        self.panel = wx.Panel(self)

        ## Status Bar for GUI, possibly ommitted from final version
        self.statusBar = self.CreateStatusBar()

        #============
        #  DATABASE
        #============
        ## Syntax connect(server, username, password, database), connects
        #  to the database server so the file information can be stored
        self.conn = MySQLdb.connect("sql2.freemysqlhosting.net","sql260291","aS5!eU1*","sql260291")
        ## cursor for database so SQL commands can be given in python
        self.mySQL = self.conn.cursor()

        #==========
        #   MENU
        #==========
        ## Menu Bar to hold program menus
        self.menuBar = wx.MenuBar()
        ## File menu to be appended to menu bar
        self.fileMenu = wx.Menu()

        ## About menu option appended to the file menu (id, title, status)
        self.menuAbout = self.fileMenu.Append(wx.ID_ABOUT, "About", "Information about project members")
        ## Exit menu option appended to the file menu (id, title,, status)
        self.menuExit = self.fileMenu.Append(wx.ID_EXIT, "Exit", "Terminate the program")

        ## "File" menu appended to menu bar (menu, title)
        self.menuBar.Append(self.fileMenu, "File")
        ## menuBar set as Menu Bar for frame
        self.SetMenuBar(self.menuBar)

        #===========
        #  BUTTONS
        #===========
        ## Button that will initiate organization process (panel, id, title, size)
        self.organizeButton = wx.Button(self.panel, wx.ID_ANY, "Organize Media!", size=(200, 100))
        ## Button that will allow users to select a folder (panel, id, title)
        self.browseButton = wx.Button(self.panel, wx.ID_ANY, "Browse")

        ## Static text to give users information on program functionality
        self.infoText = wx.StaticText(self.panel, wx.ID_ANY, "WARNING: If you music does not have proper ID3 Tags\nthis organizer will not properly work.")

        #============
        #  CHECKBOX
        #============
        ## Static text asking users to select media to be organized
        self.chooseText = wx.StaticText(self.panel, wx.ID_ANY, "Choose which media to Organize:")
        ## Check Box that indicates users desire to organize music
        self.musicCheck = wx.CheckBox(self.panel, wx.ID_ANY, "Music")
        ## Check Box that indicates users desire to organize video
        self.vidCheck = wx.CheckBox(self.panel, wx.ID_ANY, "Video")
        ## Check Box that indicates users desire to organize images
        self.imgCheck = wx.CheckBox(self.panel, wx.ID_ANY, "Images")

        #=================
        #  FOLDER SELECT
        #=================
        ## Static text asking user to enter folder path
        self.inputText = wx.StaticText(self.panel, wx.ID_ANY, "Enter Folder Path:")
        ## Input box filled in manually or auto-filled by browse button (selectDir())
        self.inputBox = wx.TextCtrl(self.panel, wx.ID_ANY, "")

        #==========
        #  EVENTS
        #==========
        ## Binds the "About" menu option to the onAbout() function
        self.Bind(wx.EVT_MENU, self.onAbout, self.menuAbout)
        ## Binds the "Exit" menu option to the onExit() function
        self.Bind(wx.EVT_MENU, self.onExit, self.menuExit)
        ## Binds the Organize Button to the organizeMedia() function
        self.Bind(wx.EVT_BUTTON, self.organizeMedia, self.organizeButton)
        ## Binds the Browse Button to the selectDir() function
        self.Bind(wx.EVT_BUTTON, self.selectDir, self.browseButton)

        #===============
        #  AUTO SIZING
        #===============
        ## Section that has program functionality info and Checkboxes
        #  organized vertically
        self.infoSizer = wx.BoxSizer(wx.VERTICAL)
        ## Adds infoText to infoSizer section (item, proportion)
        self.infoSizer.Add(self.infoText, 0)
        ## Adds chooseText to infoSizer section (item, proportion, border, spacing)
        self.infoSizer.Add(self.chooseText, 0, wx.TOP, 35)
        ## Adds musicCheck (music checkbox) to infoSizer section (item, proportion)
        self.infoSizer.Add(self.musicCheck, 0)
        ## Adds vidCheck (video checkbox) to infoSizer section (item, proportion)
        self.infoSizer.Add(self.vidCheck, 0)
        ## Adds imgCheck (image checkbox) to infoSizer section (item, proportion)
        self.infoSizer.Add(self.imgCheck, 0)

        ## Section that deals with folder selection, organized horizontally
        self.inputSizer = wx.BoxSizer(wx.HORIZONTAL)
        ## Adds inputText to inputSizer section (item, proportion, border, spacing)
        #  2 proportion gives input text 40% of remaining space (after button placed)
        #  5 spacing added to top and right to center on input box
        self.inputSizer.Add(self.inputText, 2, wx.RIGHT | wx.TOP, 5)
        ## Adds inputBox to inputSizer section (item, proportion, border, spacing)
        #  3 proportion gives input box 60% of remaining space (after button placed)
        #  3 spacing added to right and top to center on browse button
        self.inputSizer.Add(self.inputBox, 3, wx.RIGHT | wx.Top, 3)
        ## Adds browseButton to inputSizer section (item, proportion)
        #  0 proportion makes browseButton keeps it's given size (size of text as default)
        self.inputSizer.Add(self.browseButton, 0)

        ## Sizer that organizes all sections vertically
        self.orgSizer = wx.BoxSizer(wx.VERTICAL)
        ## Adds infoSizer section to orgSizer (item, proportion, border/orientation, spacing)
        #  0 proportion makes infoSizer section keep it's actual size
        #  15 spacing added to top and centered
        self.orgSizer.Add(self.infoSizer, 0, wx.TOP | wx.CENTER, 15)
        ## Adds a static line the splits 2 sections (item, proportion, border/orientation, spacing)
        #  0 proportion prevents line from re-sizing
        #  15 spacing added to all sides and line expanded across the window
        self.orgSizer.Add(wx.StaticLine(self.panel), 0, wx.ALL | wx.EXPAND, 15)
        ## Adds inputSizer section to orgSizer (item, proportion, orientation)
        #  0 proportion prevents section from resizing vertically
        #  No spacing needed due to static line spacing, section centered
        self.orgSizer.Add(self.inputSizer, 0, wx.CENTER)
        ## Adds a static line the splits 2 sections (item, proportion, border/orientation, spacing)
        #  0 proportion prevents line from re-sizing
        #  15 spacing added to all sides and line expanded across the window
        self.orgSizer.Add(wx.StaticLine(self.panel), 0, wx.ALL | wx.EXPAND, 15)
        ## Adds organizeButton to orgSizer (item, proportion, border/orientation, spacing)
        #  0 proportion keeps button at given size
        #  10 spacing added to bottom and centered
        self.orgSizer.Add(self.organizeButton, 0, wx.BOTTOM | wx.CENTER, 10)
        ## Sets sizer for our main panel to orgSizer
        self.panel.SetSizer(self.orgSizer)

        ## Makes window show up in center of screen when opened
        self.Centre()
        ## Tells program to show window when it is created
        self.Show(True)

    ## onAbout event opens a window to display names of group members
    #  when menuAbout event is triggered
    def onAbout(self, event):
        ## Creates a message window (self, message, title, button style)
        dlg = wx.MessageDialog(self, "Members: \nChrsitopher Flauta \nClayton Davis \nJing Guo \nSterling Vangeloff", "Project Contributors", wx.OK)
        ## Shows the dlg message window
        dlg.ShowModal()
        ## Destroys dlg when finished
        dlg.Destroy()

    ## organizeMedia event that does all the heavy lifting.
    #  Triggered by organzieButton event (clicking "Organize Now!" button),
    #  organizeMedia will take the value of inputBox (folder path) and
    #  use read and sort modules to get file data, store file data into
    #  database, and organized files based on user inputs.
    def organizeMedia(self, event):
        ## Variable that holds the value of inputBox (folder path)
        dest = frame.inputBox.GetValue()
        ## Check if given folder is a valid folder, if not, give user
        #  an error message
        if not os.path.exists(dest):
            ## Creates a message window (self, message, title, button style)
            dlg = wx.MessageDialog(self, "You have selected an invalid folder. \
                        please make sure the folder exists and try again.", "Invalid Folder", wx.OK)
            ## Shows the dlg message window
            dlg.ShowModal()
            ## Destroys dlg when finished
            dlg.Destroy()
        ## Check if at least one media type is selected, if not, give
        #  user an error message
        elif not (self.musicCheck.IsChecked() or self.vidCheck.IsChecked()\
                    or self.imgCheck.IsChecked()):
            ## Creates a message window (self, message, title, button style)
            dlg = wx.MessageDialog(self, "You did not select a media to be \
                        organized. Please check at least one and try again.", "Invalid Folder", wx.OK)
            ## Shows the dlg message window
            dlg.ShowModal()
            ## Destroys dlg when finished
            dlg.Destroy()
        else:
            # FIRST: Get file data and Store in Database
            subprocess.call("python read_files.py " + dest, shell=True)

            ## Check to ensure that the music checkbox is checked,
            #  and folder path not left blank
            if self.musicCheck.IsChecked() and dest != "":
                ## Music class object with folder destination and database
                music = sort.Music(dest, self.mySQL)
                ## Uses sortMusic() function from Music class in sort module
                #  to sort all files in music table of database into Artist/Album
                #  folder architecture
                music.sortMusic()

            ## Check to ensure that the video checkbox is checked,
            #  and folder path not left blank
            if self.vidCheck.IsChecked() and dest != "":
                ## Video class object with folder destination and database
                video = sort.Video(dest, self.mySQL)
                ## Uses sortVideo() function from Video class in sort module
                #  to consolidate all files in video table into a Video folder
                video.sortVideo()

            ## Check to ensure that the image checkbox is checked,
            #  and folder path not left blank
            if self.imgCheck.IsChecked() and dest != "":
                ## Image class object with folder destination and database
                image = sort.Image(dest, self.mySQL)
                ## Uses sortImage() function from Image class in sort module
                #  to consolidate all files in image table into an Image folder
                image.sortImage()

            # THIRD: Delete Empty Directories
            cleanUp = rmtest.Clean()
            cleanUp.removeEmptyFolders(dest)

            ## Message Box informing the user that the organization of their
            #  media is complete
            dlg = wx.MessageDialog(self, "Organization Complete!", "Finished", wx.OK)
            ## Shows the dlg message window
            dlg.ShowModal()
            ## Destroys dlg when finished
            dlg.Destroy()

    ## selectDir event that allows user to select a desired folder.
    #  Triggered by using the browseButton instead of manually entering
    #  folder path. This event will let a user select a valid folder
    #  and automatically enter it's path into the inputBox
    def selectDir(self, event):
        ## Opens a directory dialog box allowing users to select a
        #  directory of their choice (wx.DD_DIR_MUST_EXIST ensures it exists)
        dlg = wx.DirDialog(self, "Choose a Folder:", style=wx.DD_DIR_MUST_EXIST)
        ## Check if the "OK" button was pressed after folder was selected.
        #  Will not run if users selects cancel.
        if dlg.ShowModal() == wx.ID_OK:
            ## Variable path holds the path to the chosen directory
            path = dlg.GetPath()
            ## Uses path variable to change text inside inputBox
            frame.inputBox.ChangeValue(path)
        ## Destroys dlg when finished
        dlg.Destroy()

    ## onExit event that closes the program when user selects
    #  Exit in the file menu
    def onExit(self, event):
        ## Closes the program
        self.Close(True)


app = wx.App(False)
frame = mainWindow(None, "Media Organizer")
app.MainLoop()
