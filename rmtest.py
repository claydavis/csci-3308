#! /usr/bin/env python
import os

class Clean:
     
    def removeEmptyFolders(self, path):
        # remove empty subfolders
        files = os.listdir(path)
        if len(files):
            for f in files:
                fullpath = os.path.join(path, f)
                if os.path.isdir(fullpath):
                    self.removeEmptyFolders(fullpath)

        # if folder empty, delete it
        files = os.listdir(path)
        if len(files) == 0:
            print "Removing empty folder:", path
            os.rmdir(path)
