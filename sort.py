## @package sort
#  sort module used by GUI to organize the users media.
#  There are 3 classes; Music, Video, and Image each of which
#  support the organization of a certain medium. Classes require
#  a destination folder and a database in order to complete the
#  organization process.
import os

## Music class used by GUI to organize audio files.
#  Requires 2 parameters, a target folder, and a database
#  containing file information
class Music:

    ## Constructor that recieves a directory being sorted
    #  and the database with file information.
    #  @param directory is the folder user wants organized
    #  @param database is the database containing file info
    def __init__(self, directory, database):
        ## Variable containing target directory
        self.directory = directory
        ## Database with file information
        self.mySQL = database

    ## sortMusic function to move files into organized directories.
    #  First Music table data is read into a variable and once finsihed
    #  each file is placed in the proper folder using the Artis/Album
    #  folder architecture.
    def sortMusic(self):
        ## Gets data from Music table in database
        self.mySQL.execute("SELECT * FROM Music")
        ## Variable that holds all information in the Music table
        music = self.mySQL.fetchall()

        ## Loop runs for every row of the table, creating folders when
        #  neccessary and placing files into folders using thing
        #  Artis/Album folder architecture.
        for eachRow in music:
            ## Variable that holds the name of a file (ie "song.mp3").
            #  Variable is created so that array indexes 3 and 4 are
            #  only accessed once (optimization).
            filename = eachRow[3] + "." + eachRow[4]
            ## Variable that holds the path to the destination folder,
            #  /Music/Artis/Album. Variable created so array indexes 1 and 2
            #  are only accessed once (optimization).
            destination = self.directory + "/Music/" + eachRow[1] \
                                + "/" + eachRow[2]

            ## if statement that checks if the destination folder exists.
            #  If the destination folder does not exists, it will be created
            if not os.path.exists(destination):
                ## os.makedirs(path) will create every missing folder in the
                #  "path". Code may not be optimized.
                os.makedirs(destination)

            ## Calls rename function to move file into desired destination.
            os.rename(eachRow[5], destination + "/" + filename)

## Video class used by GUI to organize video files.
#  Requires 2 parameters, a target folder, and a database
#  containing file information
class Video:
    ## Constructor that recieves a directory being sorted
    #  and the database with file information.
    #  @param directory is the folder user wants organized
    #  @param database is the database containing file info
    def __init__(self, directory, database):
        ## Variable containing target directory
        self.directory = directory
        ## Database with file information
        self.mySQL = database

    ## sortVideo function to consolidate video files. File information
    #  is read from the database then all video files are moved into
    #  a single folder titled "Video".
    def sortVideo(self):
        ## Get data from Video table in database
        self.mySQL.execute("SELECT * FROM Video")
        ## Variable that holds all information from Video table
        video = self.mySQL.fetchall()

        ## Check to see if Video folder exists. If it doesn't, then
        #  it is created. Done outside of loop since all video files
        #  are placed into the same folder.
        if not os.path.exists(self.directory + "/Video"):
            ## Creates the "Video" folder in the destination directory
            os.mkdir(self.directory + "/Video")

        ## Loop runs for each row of the table, moving the video file to
        #  the Video folder.
        for eachRow in video:
            ## Moves file from source to destination to Video folder
            os.rename(eachRow[3], self.directory + "/Video/" + eachRow[1] + "." + eachRow[2])

## Image class used by GUI to organize image files.
#  Requires 2 parameters, a target folder, and a database
#  containing file information
class Image:
    ## Constructor that recieves a directory being sorted
    #  and the database with file information.
    #  @param directory is the folder user wants organized
    #  @param database is the database containing file info
    def __init__(self, directory, database):
        ## Variable containing target directory
        self.directory = directory
        ## Database with file information
        self.mySQL = database

    ## sortImage function to consolidate image files. File information
    #  is read from the database then all image files are moved into
    #  a single folder titled "Images".
    def sortImage(self):
        ## Get data from Images table in database
        self.mySQL.execute("SELECT * FROM Images")
        ## Variable used to store information from Image table
        images = self.mySQL.fetchall()

        ## Check to see if Images folder exists. If it doesn't, then
        #  it is created. Done outside of loop since all image files
        #  are placed into the same folder.
        if not os.path.exists(self.directory + "/Images"):
            ## Creates the "Images" folder in the destination directory
            os.mkdir(self.directory + "/Images")

        ## Loop runs for each row of the table, moving Images to
        #  the Images folder
        for eachRow in images:
            ## Move image to the "Images" folder
            os.rename(eachRow[3], self.directory + "/Images/" + eachRow[1] + "." + eachRow[2])
